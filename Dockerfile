# Use the official httpd image as the base image
FROM httpd:latest

# Copy the files to the appropriate directory in the container
COPY ./index.html /usr/local/apache2/htdocs/
COPY ./script.js /usr/local/apache2/htdocs/
COPY ./styles.css /usr/local/apache2/htdocs/

# Set the ServerName directive
RUN echo "ServerName localhost" >> /usr/local/apache2/conf/httpd.conf